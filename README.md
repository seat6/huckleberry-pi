# huckleberry pi

huckleberry-pi is a project to build a fully open source single board computer.  
The emphasis of the project will be learning, and so only open source tools and handsolderable (exposed pads) chipsets will be used.  
Additionally Huckleberry pi will be easily reproduceable, and a great way to learn about building an embedded linux system from the ground up!
See the [Project WIKI](https://gitlab.com/seat6/huckleberry-pi/-/wikis/home), for a complete guide on building this board, every step of the way.

## Tools needed

* KiCAD (free and open source)

## Features 

* SP7021 CPU
* ICE40 FPGA
* 2 Ethernet Ports
* HDMI from CPU
* HDMI from FPGA
* 4GB eMMC storage
* 2 USB-A ports
* USB Micro port for both CPU and FPGA
* TPM 1.2 
* MIDI camera connector
* 12V barrel connector
* Micro SD card slot