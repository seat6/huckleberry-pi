EESchema Schematic File Version 4
LIBS:huckleberry_pi-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Huckleberry Pi"
Date "2020-03-26"
Rev "0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5050 4100 750  350 
U 5E74ACE7
F0 "CPU" 50
F1 "CPI.sch" 50
$EndSheet
$Sheet
S 2500 4100 1200 600 
U 5E754FE4
F0 "power" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 4000 4100 900  650 
U 5E755028
F0 "peripherals" 50
F1 "peripherals.sch" 50
$EndSheet
$Sheet
S 1050 4100 1200 800 
U 5EB41A9A
F0 "ethernet" 50
F1 "ethernet.sch" 50
$EndSheet
$Sheet
S 6150 4150 1150 500 
U 5EC9AF07
F0 "FPGA" 50
F1 "FPGA.sch" 50
$EndSheet
$Comp
L Mechanical:MountingHole H4
U 1 1 5ED6261C
P 4450 2850
F 0 "H4" H 4550 2850 50  0000 L CNN
F 1 "MountingHole" H 4550 2805 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 4450 2850 50  0001 C CNN
F 3 "~" H 4450 2850 50  0001 C CNN
	1    4450 2850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5ED626D7
P 4100 2850
F 0 "H2" H 4200 2850 50  0000 L CNN
F 1 "MountingHole" H 4200 2805 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 4100 2850 50  0001 C CNN
F 3 "~" H 4100 2850 50  0001 C CNN
	1    4100 2850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5ED6298C
P 4100 2700
F 0 "H1" H 4200 2700 50  0000 L CNN
F 1 "MountingHole" H 4200 2655 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 4100 2700 50  0001 C CNN
F 3 "~" H 4100 2700 50  0001 C CNN
	1    4100 2700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5ED62B84
P 4450 2700
F 0 "H3" H 4550 2700 50  0000 L CNN
F 1 "MountingHole" H 4550 2655 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 4450 2700 50  0001 C CNN
F 3 "~" H 4450 2700 50  0001 C CNN
	1    4450 2700
	1    0    0    -1  
$EndComp
Text Notes 4050 2600 0    50   ~ 0
Mounting Holes
Wire Notes Line
	4650 2950 4000 2950
Wire Notes Line
	4000 2950 4000 2500
Wire Notes Line
	4000 2500 4650 2500
Wire Notes Line
	4650 2500 4650 2950
$Comp
L Mechanical:MountingHole H5
U 1 1 5EF9234B
P 7850 800
F 0 "H5" H 7950 846 50  0001 L CNN
F 1 "MountingHole" H 7950 755 50  0001 L CNN
F 2 "footprints:huckleberry_pi_logo2" H 7850 800 50  0001 C CNN
F 3 "~" H 7850 800 50  0001 C CNN
	1    7850 800 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 5EF926E9
P 8400 800
F 0 "H6" H 8500 846 50  0001 L CNN
F 1 "MountingHole" H 8500 755 50  0001 L CNN
F 2 "footprints:seat6_logo" H 8400 800 50  0001 C CNN
F 3 "~" H 8400 800 50  0001 C CNN
	1    8400 800 
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Large #LOGO1
U 1 1 5EF92C06
P 8100 1450
F 0 "#LOGO1" H 8100 1950 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 8100 1050 50  0001 C CNN
F 2 "" H 8100 1450 50  0001 C CNN
F 3 "~" H 8100 1450 50  0001 C CNN
	1    8100 1450
	1    0    0    -1  
$EndComp
Wire Notes Line
	7400 700  8750 700 
Wire Notes Line
	8750 700  8750 2250
Wire Notes Line
	8750 2250 7400 2250
Wire Notes Line
	7400 2250 7400 700 
Text Notes 7950 2050 0    50   ~ 0
Logo
$EndSCHEMATC
